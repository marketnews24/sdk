<?php

namespace VideoTarget;

use Psr\Log\LoggerInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\AbstractAdapter;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\CacheItem;
use Symfony\Component\HttpFoundation\Session\Session;

class Client
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var AbstractAdapter
     */
    protected $cache;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct($options, $session, $logger)
    {
        $this->options = $options;
        $this->session = $session;
        $this->logger = $logger;

        /** @see https://symfony.com/doc/current/components/cache/cache_pools.html */
        $this->cache = new FilesystemAdapter('', $options['cache_lifetime'], $options['cache_directory']);
    }

    public static function create($options, $session = null)
    {
        $options = array_merge([
            'uri'             => '',
            'auth'            => [
                'username' => '',
                'password' => '',
            ],
            'cache_lifetime'  => 3600,
            'cache_directory' => __DIR__ . '/../../var/cache',
        ], $options);

        if (!$session) {
            $session = new Session();
        }

        return new static($options, $session);
    }

    /**
     * Prepare request url
     *
     * @param string $uri
     * @param array  $data
     *
     * @return string
     */
    private function prepareUrl(string $uri, $data = []): string
    {
        $url = sprintf('%s%s',
            $this->options['uri'],
            $uri
        );

        if (count($data) > 0) {
            $url .= '?' . http_build_query($data);
        }

        return $url;
    }

    private function getNewToken()
    {
        // generate new token
        $response = $this->post('/api/login_check', [], [
            'username' => $this->options['auth']['username'],
            'password' => $this->options['auth']['password'],
        ]);

        $body = $response->getBody();

        if (isset($body->token)) {
            $this->session->set('api_access_token', $body->token);

            return $body->token;
        }

        return isset($body->error_description)
            ? $body->error_description
            : false;
    }

    public function getApiAccessToken()
    {
        $token = $this->session->get('api_access_token', null);

        // try use ping, return token, if response code == 200
        if ($token !== null) {
            /* @var $response Response */
            $response = $this->send('/api/access/ping', 'GET', ['authorization_token' => $token]);

            return $response->getCode() != 200
                ? $this->getNewToken()
                : $token;
        }

        return $this->getNewToken();
    }

    /**
     * @param       $url
     * @param       $method
     * @param array $options
     * @param array $data
     *
     * @return Response
     */
    private function send($url, $method, $options = [], $data = [])
    {
        $logger = $this->logger;

        try {
            $key = sha1(serialize([
                'url'     => $url,
                'method'  => $method,
                'options' => $options,
                'data'    => $data,
            ]));
            /** @var CacheItem $cacheItem */
            $cacheItem = $this->cache->getItem($key);
        } catch (InvalidArgumentException $exception) {
            $cacheItem = null;
            //@todo
        }

        if (isset($options['disable_cache']) && $options['disable_cache']) {
            $cacheItem = null;
        }

        if (strtolower($method) == 'get' && $cacheItem && $cacheItem->isHit()) {
            extract($cacheItem->get());
        } else {
            $curlUrl = $this->prepareUrl($url, $data);

            $logger->info(sprintf('[%s] send request - %s', $method, $curlUrl));
            $logger->info(json_encode($data));

            ob_start();
            $out = fopen('php://output', 'w');
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $curlUrl);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

            $headers = [];
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Accept: application/json';

            if (isset($options['authorization_token'])) {
                $headers[] = 'Authorization: Bearer ' . $options['authorization_token'];
            }

            if (isset($data) && count($data) > 0 && $method == 'POST') {
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            }
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_STDERR, $out);

            $response = curl_exec($ch);

            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $header = substr($response, 0, $header_size);
            $body = substr($response, $header_size);

            fclose($out);
            $debug = ob_get_clean();

            $logger->info($header_size);
            $logger->info($httpcode);
            $logger->info($header);
            $logger->info($body);

            if ($cacheItem) {
                $cacheItem->set([
                    'httpcode' => $httpcode,
                    'header'   => $header,
                    'body'     => $body,
                ]);
                $this->cache->save($cacheItem);
            }
        }

        return new Response($httpcode, $header, $body);
    }

    /**
     * @param string $url
     * @param array  $options
     *
     * @return Response
     */
    public function get(string $url, $options = [], $data = [])
    {
        return $this->send($url, 'GET', $options, $data);
    }

    /**
     * @param string $url
     * @param array  $options
     * @param array  $data
     *
     * @return Response
     */
    public function post(string $url, $options = [], $data = [])
    {
        return $this->send($url, 'POST', $options, $data);
    }

    /**
     * @param string $url
     * @param array  $options
     *
     * @return Response
     */
    public function patch(string $url, $options = [], $data = [])
    {
        return $this->send($url, 'PATCH', $options, $data);
    }

    /**
     * @param string $url
     * @param array  $options
     *
     * @return Response
     */
    public function delete(string $url, $options = [])
    {
        return $this->send($url, 'DELETE', $options);
    }

    /**
     * @param integer $limit
     * @param array   $params
     *
     * @return array|null
     */
    public function getLastContents($limit, $params = [])
    {
        $token = $this->getApiAccessToken();
        $params['limit'] = $limit;
        $response = $this->get('/api/contents', ['authorization_token' => $token], $params);
        if ($response->getCode() == 200)
            return $response->getBody();
        else
            return null;
    }
}