<?php

namespace VideoTarget;

class Response
{
    /**
     * @var integer
     */
    private $code;

    /**
     * @var string
     */
    private $headers;

    /**
     * @var string
     */
    private $body;

    public function __construct($code, $headers, $body)
    {
        $this->code = $code;
        $this->headers = $headers;
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getHeaders(): string
    {
        return $this->headers;
    }

    /**
     * @param string $headers
     */
    public function setHeaders(string $headers): void
    {
        $this->headers = $headers;
    }

    /**
     * @param bool $json_decode
     *
     * @return string|array
     */
    public function getBody($json_decode = true)
    {
        return $json_decode
            ? json_decode($this->body)
            : $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body): void
    {
        $this->body = $body;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code): void
    {
        $this->code = $code;
    }


}